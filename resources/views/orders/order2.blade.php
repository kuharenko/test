@extends('layouts.app-boot')

@section('content')
    <div class="container content-block">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Заявка 2</h4></div>
                <div class="panel-body">
                    <div class="container">
                        {{Form::model($item, ['url' => '/orders2', 'class' => 'form-horizontal', 'id'=>'order'])}}
                        <div class="form-group">
                            {{Form::label('name', "Name:", ['class' => "col-sm-2"])}}
                            <div class="col-sm-10">
                                {{Form::text('name','test' ,[ 'required'=>true])}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{Form::label('surname', "surname:", ['class' => "col-sm-2"])}}
                            <div class="col-sm-10">
                                {{Form::text('surname', 'test' ,[ 'required'=>true])}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{Form::label('email', "email:", ['class' => "col-sm-2"])}}
                            <div class="col-sm-10">
                                {{Form::email('email', 'test@test.test' ,[ 'required'=>true])}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{Form::label('phone', "phone:", ['class' => "col-sm-2"])}}
                            <div class="col-sm-10">
                                {{Form::text('phone', 'test' ,[ 'required'=>true])}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{Form::label('study',"study:", ['class' => "col-sm-2"])}}
                            <div class="col-sm-2">
                                {{Form::select('study', [
                                1=>'Bachelor',
                                2=>'Master',
                                3=>'PhD',
                                ], null, ['class' => 'form-control'])}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{Form::label('picture', "picture:", ['class' => "col-sm-2"])}}
                            <div class="col-sm-10">
                                {{Form::file('picture',[ 'required'=>true])}}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                {{Form::submit('Send',['class' => 'btn btn-primary'])}}
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                    <div class="ajax-success">
                        <div class="alert alert-success" role="alert">Well done</div>
                    </div>
                    <div class="ajax-fail">
                        <div class="alert alert-danger" role="alert">Wrong</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

