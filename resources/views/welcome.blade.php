@extends('layouts.app-boot')

@section('content')
    <div class="container content-block">
        <div class="col-xs-10">
            <div class="row">
                @foreach($items as $item)

                    <div class="col-sm-6 col-md-4">
                        <a href="#ex{{$item->id}}" rel="modal:open">
                            <div class="thumbnail">
                                <img src="{{ asset($item->img_url) }}" width="100%" height="200px" style="min-height: 200px"
                                     alt="...">
                                <div class="caption">
                                    <h3>{{ $item->title }}</h3>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div id="ex{{$item->id}}" class="modal">
                        <img src="{{ asset($item->img_url) }}" width="100%" alt="...">
                        <a href="#" rel="modal:close"></a>
                    </div>

                @endforeach
                {{ $items->links() }}
            </div>
        </div>
    </div>
@stop
