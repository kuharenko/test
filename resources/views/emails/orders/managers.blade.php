@component('mail::message')
    У вас новая заявка
    <br>
    <br>

    @foreach($form as $field => $value)
        <p>{{$field}}: {{$value}}</p>
    @endforeach

Thanks,<br>
{{ config('app.name') }}
@endcomponent
