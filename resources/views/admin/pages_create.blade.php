@extends('adminlte::page')

@section('title', 'Create')


@section('content_header')
    <h2>Создание контента</h2>
@stop

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><h4>Контент</h4></div>
        <div class="panel-body">
            <div class="container">
                {{Form::model($item, ['url' => '/panel/pages', 'class' => 'form-horizontal'])}}
                <div class="form-group">
                    {{Form::label('title', "Title:", ['class' => "col-sm-2"])}}
                    <div class="col-sm-10">
                        {{Form::text('title')}}
                    </div>
                </div>

                <div class="form-group">
                    {{Form::label('img_url', "Image:", ['class' => "col-sm-2"])}}
                    <div class="col-sm-2">
                        <div class="input-group">
                           <span class="input-group-btn">
                             <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                               <i class="fa fa-picture-o"></i> Choose
                             </a>
                           </span>
                            <input id="thumbnail" class="form-control" type="text" name="img_url">
                        </div>
                    </div>
                    <img id="holder" src="" style="margin-top:15px;max-height:100px;">
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{Form::submit('Coхранить',['class' => 'btn btn-primary'])}}
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript" src="{{ asset('vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script>
        $('#lfm').filemanager('image');
    </script>
@stop
