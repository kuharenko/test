@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Pages</h1>
@stop

@section('content')

    <a href="/panel/pages/create" type="button" class="btn btn-success">Create</a>

    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Заголовок</th>
            <th scope="col">Картинка</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $key => $item)

            <tr>
                <th scope="row">{{ ++$key }}</th>
                <td>{{ $item->title }}</td>
                <td><img src="{{ asset($item->img_url ) }}" alt="" width="100px"></td>
                <td>
                    <div class="col-xs-2">
                        <a href="/panel/pages/edit/{{$item->id}}" type="button" class="btn btn-warning">Edit</a>
                    </div>
                    <div class="col-xs-2">
                       {{ Form::open(['url' => '/panel/pages/delete/'.$item->id, 'method' => 'post']) }}
                       <button type="submit" class="btn btn-danger">Delete</button>
                       {{ Form::close() }}
                   </div>
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
@stop
