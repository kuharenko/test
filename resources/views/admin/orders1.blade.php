@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Orders 1</h1>
@stop

@section('content')

    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">name</th>
            <th scope="col">surname</th>
            <th scope="col">email</th>
            <th scope="col">phone</th>
            <th scope="col">study</th>
            <th scope="col">picture</th>
            @role('super_admin')
            <th scope="col">ip</th>
            <th scope="col">utm</th>
            <th scope="col"></th>
            @endrole
        </tr>
        </thead>
        <tbody>
        @foreach($items as $key => $item)

            <tr>
                <th scope="row">{{ ++$key }}</th>
                <td>{{ $item->name }}</td>
                <td>{{ $item->surname }}</td>
                <td>{{ $item->email }}</td>
                <td>{{ $item->phone }}</td>
                <td>{{ $item->study }}</td>

                <td><img src="{{ implode('',explode('public/',asset('storage/'.$item->picture )))}}" alt="" width="100px"></td>
                @role('super_admin')
                <td>{{ $item->ip }}</td>
                <td>{{ $item->utm }}</td>
                <td>
                        <div class="col-xs-2">
                           {{ Form::open(['url' => '/panel/orders1/delete/'.$item->id, 'method' => 'post']) }}
                           <button type="submit" class="btn btn-danger">Delete</button>
                           {{ Form::close() }}
                       </div>
                </td>
                @endrole
            </tr>

        @endforeach
        </tbody>
    </table>
@stop
