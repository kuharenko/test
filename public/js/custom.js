$(function() {
    $('.pop').on('click', function() {
        $('.imagepreview').attr('src', $(this).find('img').attr('src'));
        $('#imagemodal').modal('show');
    });

    function showRequest() {
        $('.ajax-success').hide();
        $('.ajax-fail').hide();
    }

    function showResponse(responseText)  {

        if(responseText)
        {
            $('.ajax-success').show();
        }else{
            $('.ajax-fail').show();
        }
    }

    var options = {
        beforeSubmit:  showRequest,
        success:       showResponse
    };

    $('#order').ajaxForm(options);
});

