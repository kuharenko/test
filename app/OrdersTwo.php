<?php

namespace App;

use App\Jobs\SendManagersTwoEmail;
use App\Jobs\SendUserEmail;
use Illuminate\Database\Eloquent\Model;

class OrdersTwo extends Model
{
    protected $table ='orders_two';

    protected $fillable = [
        'name','surname', 'email','phone','study', 'picture','ip','utm'
    ];

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            dispatch(new SendUserEmail($model));
            dispatch(new SendManagersTwoEmail($model));
        });
    }
}
