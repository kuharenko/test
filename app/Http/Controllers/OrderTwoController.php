<?php

namespace App\Http\Controllers;

use App\OrdersTwo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OrderTwoController extends Controller
{
    public function index()
    {
        return view('admin.orders2', ['items'=> OrdersTwo::paginate()]);
    }

    public function orders()
    {
        return view('orders.order2', ['item'=>new OrdersTwo()]);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data['ip'] = $_SERVER['REMOTE_ADDR'];
        $data['utm'] = $_SERVER['QUERY_STRING'];

        $path = $request->file('picture')->store('public/photos/orders/2');

        $data['picture'] = $path;

        OrdersTwo::create($data);

        return response()->json(['status'=>'OK'], 200);
    }

    public function delete(OrdersTwo $ordersTwo)
    {
        $file = $ordersTwo->picture;
        Storage::delete($file);

        $ordersTwo->delete();

        return redirect('/panel/order2');
    }
}
