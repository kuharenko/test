<?php

namespace App\Http\Controllers;

use App\Content;
use App\Http\Requests\StoreContentRequest;
use App\Http\Requests\UpdateContentRequest;

class HomeController extends Controller
{

    public function index()
    {
        return view('welcome', ['items'=>Content::paginate(20)]);
    }




    public function second_orders()
    {
        return view('orders.order2');
    }

    public function admin()
    {
        return view('home');
    }

    public function pages()
    {
        return view('admin.pages', ['items'=>Content::paginate(30)]);
    }

    public function edit(Content $content)
    {
        return view('admin.pages_edit', ['item'=>$content]);
    }

    public function create()
    {
        return view('admin.pages_edit', ['item'=>new Content()]);
    }

    public function store(StoreContentRequest $request)
    {
        Content::create($request->validated());
        return redirect('/panel/pages');
    }

    public function update(UpdateContentRequest $request, Content $content)
    {
        $content->update($request->validated());
        return redirect('/panel/pages');
    }

    public function delete(Content $content)
    {
        $content->delete();
        return redirect('/panel/pages');
    }
}
