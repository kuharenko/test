<?php

namespace App\Http\Controllers;

use App\OrdersOne;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OrderOneController extends Controller
{
    public function index()
    {
        return view('admin.orders1', ['items'=> OrdersOne::paginate()]);
    }

    public function orders()
    {
        return view('orders.order1', ['item'=>new OrdersOne()]);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data['ip'] = $_SERVER['REMOTE_ADDR'];
        $data['utm'] = $_SERVER['QUERY_STRING'];

        $path = $request->file('picture')->store('public/photos/orders/1');

        $data['picture'] = $path;

        OrdersOne::create($data);

        return response()->json(['status'=>'OK'], 200);
    }

    public function delete(OrdersOne $ordersOne)
    {
        $file = $ordersOne->picture;
        Storage::delete($file);

        $ordersOne->delete();

        return redirect('/panel/order1');
    }
}
