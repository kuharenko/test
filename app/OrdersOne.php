<?php

namespace App;

use App\Jobs\SendManagersOneEmail;
use App\Jobs\SendUserEmail;
use Illuminate\Database\Eloquent\Model;

class OrdersOne extends Model
{
    protected $table ='orders_one';

    protected $fillable = [
        'name','surname', 'email','phone','study', 'picture','ip','utm'
    ];

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
           dispatch(new SendUserEmail($model));
           dispatch(new SendManagersOneEmail($model));
        });
    }
}
