<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/order1', 'OrderOneController@orders');
Route::get('/order2', 'OrderTwoController@orders');

Route::group(['prefix' => 'panel', 'middleware' => ['role:manager1|manager2|super_admin']], function () {
    Route::get('/', 'HomeController@admin')->name('home');
    Route::get('/pages', 'HomeController@pages')->name('pages');
    Route::get('/pages/edit/{content}', 'HomeController@edit')->name('edit');
    Route::get('/pages/create', 'HomeController@create')->name('create');

    Route::post('/pages/{content}', 'HomeController@update')->name('update');
    Route::post('/pages/delete/{content}', 'HomeController@delete')->name('delete');
    Route::post('/pages', 'HomeController@store')->name('store');

    Route::group(['middleware' => ['role:manager1|super_admin']], function () {
        Route::get('/order1', 'OrderOneController@index')->name('order1');
    });

    Route::group(['middleware' => ['role:manager2|super_admin']], function () {
        Route::get('/order2', 'OrderTwoController@index')->name('order2');
    });

    Route::group(['middleware' => ['role:super_admin']], function () {
        Route::post('/orders1/delete/{ordersOne}', 'OrderOneController@delete')->name('delete_order1');
        Route::post('/orders2/delete/{ordersTwo}', 'OrderTwoController@delete')->name('delete_order2');
    });
});

Route::post('/orders1', 'OrderOneController@store')->name('store_order1');
Route::post('/orders2', 'OrderTwoController@store')->name('store_order2');

