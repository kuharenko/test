<?php

use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contents')->insert([
            [
                'id' => 1,
                'title' => 'test',
                'img_url' => '/photos/1/_7themes-su.jpg',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => 2,
                'title' => 'test2',
                'img_url' => '/photos/1/2000x1333-4600947-himalayas-mountains-sky-clouds-stars-night-sunset.jpg',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
